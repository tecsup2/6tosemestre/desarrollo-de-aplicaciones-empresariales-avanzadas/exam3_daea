﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolEntities;

namespace ClinicaLTS
{
    public class CPacienteLTS
    {

            LinqToSqlDataContext context = new LinqToSqlDataContext();

        public List<CPaciente> Mostrar()
        {
            List<CPaciente> lstPaciente = new List<CPaciente>();

            var query = from p in context.Paciente select p;

            foreach(var paciente in query)
            {
                CPaciente np = new CPaciente();
                np.CodigoCorrelativo = paciente.CodigoCorrelativo;
                np.CodigoOA = paciente.CodigoOA;
                np.NombreCompleto = paciente.NombreCompleto;
                np.Email= paciente.Email;
                np.Celular= paciente.Celular;
                np.Medico= paciente.Medico;
                np.Aseguradora= paciente.Aseguradora;
                np.Procedencia= paciente.Procedencia;
                np.Examen= paciente.Examen;
                np.Estado= paciente.Estado;
                np.FechaSolicitud= paciente.FechaSolicitud;
                np.Edad = (int)paciente.Edad;
                np.Imagen= paciente.Imagen;
                np.Informe = paciente.Informe;
                lstPaciente.Add(np);
            }
            return lstPaciente;
        }


        public void Insertar(CPaciente cp)
        {
            Paciente p = new Paciente();
            p.CodigoOA = cp.CodigoOA;
            p.NombreCompleto = cp.NombreCompleto;
            p.Email = cp.Email;
            p.Celular = cp.Celular;
            p.Medico = cp.Medico;
            p.Aseguradora = cp.Aseguradora;
            p.Procedencia = cp.Procedencia;
            p.Estado= cp.Estado;
            p.FechaSolicitud= cp.FechaSolicitud;
            p.Edad= cp.Edad;
            p.Imagen= cp.Imagen;
            p.Informe= cp.Informe;

            context.Paciente.InsertOnSubmit(p);
            context.SubmitChanges();
        }


        public void Modificar(CPaciente pacienteABuscar)
        {

            var cp = (from paciente in context.Paciente
                                   where paciente.NombreCompleto == pacienteABuscar.NombreCompleto
                                   select paciente 
                                   ).FirstOrDefault();


            Paciente p = new Paciente();
            p.CodigoOA = cp.CodigoOA;
            p.NombreCompleto = cp.NombreCompleto;
            p.Email = cp.Email;
            p.Celular = cp.Celular;
            p.Medico = cp.Medico;
            p.Aseguradora = cp.Aseguradora;
            p.Procedencia = cp.Procedencia;
            p.Estado= cp.Estado;
            p.FechaSolicitud= cp.FechaSolicitud;
            p.Edad= cp.Edad;
            p.Imagen= cp.Imagen;
            p.Informe= cp.Informe;

            context.Paciente.InsertOnSubmit(p);
            context.SubmitChanges();
        }


    }
}
