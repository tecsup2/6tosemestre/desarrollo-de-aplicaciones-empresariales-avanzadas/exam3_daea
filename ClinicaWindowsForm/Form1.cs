﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolEntities;
using SchoolEntities;
using ClinicaLTS;

namespace ClinicaWindowsForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CPacienteLTS lts = new CPacienteLTS();
            dgvListado.DataSource = lts.Mostrar();
            dgvListado.Refresh();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            CPacienteLTS ltsn = new CPacienteLTS();
            CPaciente np = new CPaciente();

            //(np.CodigoCorrelativo =  Int32.Parse(txtCodigoCorrelativo.Text);
            np.CodigoOA = Int32.Parse(txtCodigoQA.Text);
            np.NombreCompleto = txtNombreCompleto.Text;
            np.Email= txtEmail.Text;
            np.Celular= txtCelular.Text;
            np.Medico= txtMedico.Text;
            np.Aseguradora= txtAseguradora.Text;
            np.Procedencia= txtProcedencia.Text;
            np.Examen= txtExamen.Text;
            np.Estado= txtEstado.Text;
            np.FechaSolicitud= txtFechaSolicitud.Text;
            np.Edad = Int32.Parse(txtEdad.Text);
            np.Imagen = txtImagen.Text;
            np.Informe = txtInforme.Text;

            ltsn.Insertar(np);
            dgvListado.DataSource = ltsn.Mostrar();
            dgvListado.Refresh();
        }

        private void btnBuscarPorNombre_Click(object sender, EventArgs e)
        {

        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            CPacienteLTS ltsn = new CPacienteLTS();
            CPaciente np = new CPaciente();
            
            np.CodigoCorrelativo =  Int32.Parse(txtCodigoCorrelativo.Text);
            np.CodigoOA = Int32.Parse(txtCodigoQA.Text);
            np.NombreCompleto = txtNombreCompleto.Text;
            np.Email= txtEmail.Text;
            np.Celular= txtCelular.Text;
            np.Medico= txtMedico.Text;
            np.Aseguradora= txtAseguradora.Text;
            np.Procedencia= txtProcedencia.Text;
            np.Examen= txtExamen.Text;
            np.Estado= txtEstado.Text;
            np.FechaSolicitud= txtFechaSolicitud.Text;
            np.Edad = Int32.Parse(txtEdad.Text);
            np.Imagen = txtImagen.Text;
            np.Informe = txtInforme.Text;

            ltsn.Modificar(np);
            dgvListado.DataSource = ltsn.Mostrar();
            dgvListado.Refresh();

        }

        private void dgvListado_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvListado.SelectedRows.Count > 0)
            {
                //codcli = Int32.Parse(dgvVehiculos.SelectedRows[0].Cells[0].Value.ToString());
                //placa = dgvVehiculos.SelectedRows[0].Cells[1].Value.ToString();


                if (dgvListado.SelectedRows[0].Cells[0].Value != null)
                {
                txtCodigoCorrelativo.Text = dgvListado.SelectedRows[0].Cells[0].Value.ToString();
                }
                else
                {
                    txtCodigoCorrelativo.Text = "";
                }

                if (dgvListado.SelectedRows[0].Cells[1].Value != null)
                {
                txtCodigoQA.Text = dgvListado.SelectedRows[0].Cells[1].Value.ToString();
                }
                else
                {
                    txtCodigoQA.Text = "";
                }

                if(dgvListado.SelectedRows[0].Cells[2].Value != null)
                {
                txtNombreCompleto.Text = dgvListado.SelectedRows[0].Cells[2].Value.ToString();
                }
                else
                {
                    txtNombreCompleto.Text = "";
                }

                if(dgvListado.SelectedRows[0].Cells[3].Value != null)
                {
                txtEmail.Text = dgvListado.SelectedRows[0].Cells[3].Value.ToString();
                }
                else
                {
                    txtEmail.Text = "";
                }

                if(dgvListado.SelectedRows[0].Cells[4].Value != null)
                {
                txtCelular.Text = dgvListado.SelectedRows[0].Cells[4].Value.ToString();
                }
                else
                {
                    txtCelular.Text = "";
                }

                if(dgvListado.SelectedRows[0].Cells[5].Value != null)
                {
                txtMedico.Text = dgvListado.SelectedRows[0].Cells[5].Value.ToString();
                }
                else
                {
                    txtMedico.Text = "";
                }

                if(dgvListado.SelectedRows[0].Cells[6].Value != null)
                {
                txtAseguradora.Text = dgvListado.SelectedRows[0].Cells[6].Value.ToString();
                }
                else
                {
                    txtAseguradora.Text = "";
                }

                if(dgvListado.SelectedRows[0].Cells[7].Value != null)
                {
                txtProcedencia.Text = dgvListado.SelectedRows[0].Cells[7].Value.ToString();
                }
                else
                {
                    txtProcedencia.Text = "";
                }

                if(dgvListado.SelectedRows[0].Cells[8].Value != null)
                {
                txtExamen.Text = dgvListado.SelectedRows[0].Cells[8].Value.ToString();
                }
                else
                {
                    txtExamen.Text = "";
                }

                if(dgvListado.SelectedRows[0].Cells[9].Value != null)
                {
                txtEstado.Text = dgvListado.SelectedRows[0].Cells[9].Value.ToString();
                }
                else
                {
                    txtEstado.Text = "";
                }

                if(dgvListado.SelectedRows[0].Cells[10].Value != null)
                {
                txtFechaSolicitud.Text = dgvListado.SelectedRows[0].Cells[10].Value.ToString();
                }
                else
                {
                    txtFechaSolicitud.Text = "";
                }

                if(dgvListado.SelectedRows[0].Cells[11].Value != null)
                {
                txtEdad.Text = dgvListado.SelectedRows[0].Cells[11].Value.ToString();
                }
                else
                {
                    txtEdad.Text = "";
                }

                if(dgvListado.SelectedRows[0].Cells[12].Value != null)
                {
                txtImagen.Text = dgvListado.SelectedRows[0].Cells[12].Value.ToString();
                }
                else
                {
                    txtImagen.Text = "";
                }

                if(dgvListado.SelectedRows[0].Cells[13].Value != null)
                {
                txtInforme.Text = dgvListado.SelectedRows[0].Cells[13].Value.ToString();
                }
                else
                {
                    txtInforme.Text = "";
                }
                    


            }




        }
    }
}
