﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacienteEntities
{
    public class CPaciente
    {
        public int CodigoCorrelativo { get; set; }
        public int CodigoOA { get; set; }
        public string NombreCompleto { get; set; }
        public string Email { get; set; }
        public string Celular { get; set; }
        public string Medico { get; set; }
        public string Aseguradora { get; set; }
        public string Procedencia { get; set; }
        public string Examen { get; set; }
        public string Estado { get; set; }
        public string FechaSolicitud { get; set; }
        public int Edad { get; set; }
        public string Imagen { get; set; }
        public string Informe { get; set; }

    }
}
