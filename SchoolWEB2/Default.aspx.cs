﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SchoolEntities;
using SchoolBLL;

namespace SchoolWEB2
{
    public partial class _Default : Page
    {
        //CPersonBLL personB = new CPersonBLL();
        CPacienteBLL pacienteB = new CPacienteBLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            //gvListado.DataSource = personB.ListarBLL();
            gvListado.DataSource = pacienteB.ListarBLL();
            gvListado.DataBind();
        }
    }
}