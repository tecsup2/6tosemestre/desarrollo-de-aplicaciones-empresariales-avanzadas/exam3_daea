﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SchoolEntities;

namespace SchoolDAL
{
    public class CPersonDAL
    { 
        /*public List<CPerson> Listar()
        {
            using(SchoolEntities contexto = new SchoolEntities())
            {
                var query = contexto.Person.Select(p => new CPerson
                {
                    PersonID = p.PersonID,
                    LastName = p.LastName,
                    FirstName = p.FirstName,
                });
                return query.ToList();
            }
        }*/

        public List<CPaciente> ListarPacientes()
        {
            using(SchoolEntities contexto = new SchoolEntities())
            {
                var query = contexto.Paciente.Select(p => new CPaciente 
                {
                    CodigoCorrelativo = p.CodigoCorrelativo,
                    CodigoOA = (int)p.CodigoOA,
                    NombreCompleto = p.NombreCompleto,
                    Email = p.Email,
                    Celular = p.Celular,
                    Medico = p.Medico,
                    Aseguradora = p.Aseguradora,
                    Procedencia = p.Procedencia,
                    Examen = p.Examen,
                    Estado = p.Estado,
                    FechaSolicitud = p.FechaSolicitud,
                    Edad = (int)p.Edad,
                    Informe = p.Informe,

                });
                return query.ToList();
            }
        }



    }
}
